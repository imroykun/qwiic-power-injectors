This is a set of small boards for supplying 3.3 V power onto an I2C bus connected using [QWIIC](https://www.sparkfun.com/qwiic) connectors.

[View on CadLab](https://cadlab.io/projects/qwiic-power-injectors)
