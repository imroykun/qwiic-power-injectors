# LiFePO4 14500 power

This board receives power from a small solar panel and charges a 14500 (AA) sized LiFePO4 battery.
The battery power is supplied directly to the two QWIIC connectors.
Make sure your 3.3 V devices can handle 3.65 V!

This board is currently setup to use a 6 V solar panel.
See the [BQ24650 datasheet](https://www.ti.com/lit/ds/symlink/bq24650.pdf) about changing R3 and R4 to change the MPPT voltage.
You could also change R1 and R2 to change the charge voltage for a different battery chemistry.

![Front render](render1.png)
![Back render](render2.png)
